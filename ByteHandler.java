import java.nio.charset.StandardCharsets;

public class ByteHandler {

    public static byte[] concatenateBytes (byte[] b1, byte[] b2) {
        byte[] bytes = new byte [b1.length + b2.length];
        for (int i = 0; i < b1.length;  i++) {
            bytes[i] = b1[i];
        }
        for (int i = 0; i < b2.length; i++) {
            bytes[i+b1.length] = b2[i];
        }

        return bytes;
    }

    public static boolean messEndCorrectly (byte[] bytes) {
        String control = stringFromByteArray(bytes);
        if ((control.compareTo("***") == 0) || control.compareTo("+++") == 0) {
            return true;
        }
        else {
            return false;
        }
    }

    public static int intFromByte (byte b) {
        return (b & 0xFF);
    }

    public static byte intToByte (int value) {
        return ((byte) (value));
    }
    
    //Little-Endian
    public static int intFromByteArray (byte[] bytes) {
        if (bytes.length == 0) {
            System.err.println("no bytes to convert\n");
            return 0;
        }
        double sum = 0.0;
        for (int i = 0; i < bytes.length; i++) {
            sum = sum + (intFromByte(bytes[i]) * Math.pow(16, i));
        }
        return ((int) sum);
    }

    //Little-Endian
    public static byte[] intToByteArray(int value, int length) {
        byte[] bytes = new byte[length];
        for (int i = 0; i < length; i++) {
            bytes[i] = (byte) (value >> (8*i));
        }
        return bytes;
    }

    public static char charFromByte (byte b) {
        char c = (char) (b & 0xFF);
        return c;
    } 

    public static String stringFromByteArray (byte[] bytes) {
        if (bytes.length == 0) {
            System.err.println("no bytes to convert\n");
            return null;
        }
        String string = new String(bytes, StandardCharsets.US_ASCII);
        return string;
    }

    public static byte[] stringToByteArray (String s) {
        try {
            byte[] bytes = s.getBytes(StandardCharsets.US_ASCII);
            return bytes;
        }
        catch (Exception e) {
            System.err.println(e);
            System.out.println();
            return null;
        }
    }
}
