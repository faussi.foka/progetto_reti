import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;
import java.net.*;

public class TestMulticastChannel {
    public static void main (String[] args) {
        try {
            Selector sel = Selector.open();
            NetworkInterface ni = NetworkInterface.getNetworkInterfaces().nextElement();
            DatagramChannel dsc_multicast = DatagramChannel.open(StandardProtocolFamily.INET);
            dsc_multicast.setOption(StandardSocketOptions.SO_REUSEADDR, true);
            dsc_multicast.bind(new InetSocketAddress(9999));
            dsc_multicast.setOption(StandardSocketOptions.IP_MULTICAST_IF, ni);
            dsc_multicast.join(InetAddress.getByName("225.1.2.99"), ni);
            dsc_multicast.configureBlocking(false);
            dsc_multicast.register(sel, SelectionKey.OP_READ);

            DatagramChannel dsc = DatagramChannel.open();
            dsc.configureBlocking(false);
            dsc.bind(new InetSocketAddress(4343));
            dsc.register(sel, SelectionKey.OP_READ);

            int cont = 0;

            while (cont < 5) {
                cont++;
                System.out.println("Waiting for a message...");
                sel.select();
                Iterator<SelectionKey> it = sel.selectedKeys().iterator();
                while (it.hasNext()) {
                    SelectionKey sk = it.next();
                    it.remove();
                    if (sk.isReadable() && sk.channel() == dsc_multicast) {
                        ByteBuffer buff = ByteBuffer.allocate(100);
                        System.out.println("Message Multicast received!");
                        dsc_multicast.receive(buff);
                        String s = new String(buff.array(), 0, buff.array().length);
                        buff.clear();
                        System.out.println("Message: " + s + "\n");
                    }
                    else if (sk.isReadable() && sk.channel() == dsc) {
                        ByteBuffer buff = ByteBuffer.allocate(100);
                        System.out.println("Message UDP received!");
                        dsc.receive(buff);
                        String s = new String(buff.array(), 0, buff.array().length);
                        System.out.println("Message: " + s + "\n");
                    }
                    else {
                        System.out.println("Qualcosa non va!\n");
                    }
                }
            }

            dsc_multicast.close();
            dsc.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }    
}
