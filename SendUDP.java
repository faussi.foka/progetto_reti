import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class SendUDP {
    public static void main (String[] args) {
        try {
            DatagramSocket dso = new DatagramSocket();
            byte[] data;
            while (true) {
                String id = RandomString.getRandomString(8);
                BufferedReader kb = new BufferedReader(new InputStreamReader(System.in));
                String mess = kb.readLine();
                String temp = "MESSP " + id + " " + mess + "+++";
                data = ByteHandler.stringToByteArray(temp);
                DatagramPacket packet = new DatagramPacket(data, data.length, InetAddress.getByName("localhost"), 5555);
                dso.send(packet);
            }
        }
        catch (Exception e) {
            System.err.println(e);
            e.printStackTrace();
        }
    }
}
