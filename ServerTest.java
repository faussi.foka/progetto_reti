import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.BufferOverflowException;

/*
    Uso questa classe per fare i vari test col client del progetto.

    Per usare correttamente questo "Server" di prova, implementare
    lo spazio sottostante alla scritta "Lavora col client",
    a seconda di che cosa devo testare.
*/

public class ServerTest {

    private static void listPlayersSpecificGame (BufferedInputStream br, BufferedOutputStream pw, byte[] buff) throws IOException {
        //[LIST! m s***]
        int m = ByteHandler.intFromByte(buff[6]);
        int s = 5;
        buff = ByteHandler.stringToByteArray("LIST! ");
        byte[] buff_temp = new byte [1];
        buff_temp[0] = ByteHandler.intToByte(m);
        buff = ByteHandler.concatenateBytes(buff, buff_temp);
        buff = ByteHandler.concatenateBytes(buff, ByteHandler.stringToByteArray(" "));
        buff_temp[0] = ByteHandler.intToByte(s);
        buff = ByteHandler.concatenateBytes(buff, buff_temp);
        buff = ByteHandler.concatenateBytes(buff, ByteHandler.stringToByteArray("***"));
        pw.write(buff);
        pw.flush();

        //[PLAYR id***]
        buff = null;
        buff_temp = null;
        for (int i = 0; i < s; i++) {
            String mess = "PLAYR " + RandomString.getRandomString(8) + "***";
            buff = ByteHandler.stringToByteArray(mess);
            pw.write(buff);
            pw.flush();
        }
    }

    private static void sendSecondGameMessage (BufferedInputStream br, BufferedOutputStream pw) throws IOException {
        //send [POSIT id x y***]
        String id = "AAaaBBbb";
        String mess = "POSIT " + id + " 005 005***";
        byte[] buff = ByteHandler.stringToByteArray(mess);
        pw.write(buff);
        pw.flush();
    }

    private static void sendFirstGameMessage (BufferedInputStream br, BufferedOutputStream pw) throws IOException {
        //send [WELCO m h w f ip port***]
        byte [] buff = ByteHandler.stringToByteArray("WELCO ");
        int m = 40;
        buff = ByteHandler.concatenateBytes(buff, ByteHandler.intToByteArray(m, 1));
        int h = 10;
        int w = 8;
        buff = ByteHandler.concatenateBytes(buff, ByteHandler.stringToByteArray(" "));
        buff = ByteHandler.concatenateBytes(buff, ByteHandler.intToByteArray(h, 2));
        buff = ByteHandler.concatenateBytes(buff, ByteHandler.stringToByteArray(" "));
        buff = ByteHandler.concatenateBytes(buff, ByteHandler.intToByteArray(w, 2));
        buff = ByteHandler.concatenateBytes(buff, ByteHandler.stringToByteArray(" "));
        int f = 12;
        buff = ByteHandler.concatenateBytes(buff, ByteHandler.intToByteArray(f, 1));
        String ip = "225.1.0.99#####";
        String port = "9999";
        String end_message = "***";
        String temp = " " + ip + " " + port + end_message;
        buff = ByteHandler.concatenateBytes(buff, ByteHandler.stringToByteArray(temp));
        pw.write(buff);
        pw.flush();
    }

    private static void sendFirstMessage (BufferedInputStream br, BufferedOutputStream pw) throws IOException {
        //  Mando prima il mex [GAMES␣n***]
        //  Poi n mex [OGAME␣m␣s***]
        
        int numero = 10;
        byte[] bytes_one = ByteHandler.stringToByteArray("GAMES ");
        byte bytes_two = ByteHandler.intToByte(numero);
        byte[] bytes_three = ByteHandler.stringToByteArray("***");
        byte[] buff = new byte [bytes_one.length + 1 + bytes_three.length];

        for (int i = 0; i < bytes_one.length; i++) {
            buff[i] = bytes_one[i];
        }
        buff[bytes_one.length] = bytes_two;
        for (int i = 0; i < bytes_three.length; i++) {
            buff[i + bytes_one.length + 1] = bytes_three[i];
        }
        pw.write(buff);
        pw.flush();
        

        int m = 37;
        int s = 10;

        for (int i = 0; i < numero; i++) {
            byte[] bb_one = ByteHandler.stringToByteArray("OGAME ");
            byte bb_two = ByteHandler.intToByte(m*i);
            byte[] bb_three = ByteHandler.stringToByteArray(" ");
            byte bb_four = ByteHandler.intToByte(s);
            byte[] bb_five = ByteHandler.stringToByteArray("***");

            byte[] temp = new byte[12];

            temp[0] = bb_one[0];
            temp[1] = bb_one[1];
            temp[2] = bb_one[2];
            temp[3] = bb_one[3];
            temp[4] = bb_one[4];
            temp[5] = bb_one[5];

            temp[6] = bb_two;

            temp[7] = bb_three[0];

            temp[8] = bb_four;

            temp[9] = bb_five[0];
            temp[10] = bb_five[1];
            temp[11] = bb_five[2];

            

            pw.write(temp);
            pw.flush();
        }
    }
    
    private static byte[] printReceivedMessage (BufferedInputStream br, BufferedOutputStream pw, int length) throws IOException {
        byte[] buff = new byte [length];
        for (int i = 0; i < buff.length; i++) {
            buff[i] = 0;
        }
        br.read(buff);
        System.out.println(ByteHandler.stringFromByteArray(buff));
        return (buff);
    }

    private static void joinGame (BufferedInputStream br, BufferedOutputStream pw, byte[] buff) throws IOException {
        int m = ByteHandler.intFromByte(buff[20]);
        buff = ByteHandler.stringToByteArray("REGOK ");
        byte[] temp = new byte[1];
        temp [0] = ByteHandler.intToByte(m);
        buff = ByteHandler.concatenateBytes(buff, temp);
        buff = ByteHandler.concatenateBytes(buff, ByteHandler.stringToByteArray("***"));
        pw.write(buff);
        pw.flush();
        /*
        buff = ByteHandler.stringToByteArray("REGNO***");
        pw.write(buff);
        pw.flush();
        */
    }

    private static void joinNewGame (BufferedInputStream br, BufferedOutputStream pw) throws IOException {
        int m = 40;
        byte[] buff = ByteHandler.stringToByteArray("REGOK ");
        byte[] temp = new byte[1];
        temp [0] = ByteHandler.intToByte(m);
        buff = ByteHandler.concatenateBytes(buff, temp);
        buff = ByteHandler.concatenateBytes(buff, ByteHandler.stringToByteArray("***"));
        pw.write(buff);
        pw.flush();
        /*
        buff = ByteHandler.stringToByteArray("REGNO***");
        pw.write(buff);
        pw.flush();
        */
    }

    private static void infoGame (BufferedInputStream br, BufferedOutputStream pw, byte[] buff) throws IOException {
        //Mando [SIZE! m h w***]
        int m = 40;
        int h = 10; int w = 8;
        byte[] temp = new byte [1];
        buff = ByteHandler.stringToByteArray("SIZE! ");
        temp[0] = ByteHandler.intToByte(m);
        buff = ByteHandler.concatenateBytes(buff, temp);
        buff = ByteHandler.concatenateBytes(buff, ByteHandler.stringToByteArray(" "));
        temp = ByteHandler.intToByteArray(h, 2);
        buff = ByteHandler.concatenateBytes(buff, temp);
        buff = ByteHandler.concatenateBytes(buff, ByteHandler.stringToByteArray(" "));
        temp = ByteHandler.intToByteArray(w, 2);
        buff = ByteHandler.concatenateBytes(buff, temp);
        buff = ByteHandler.concatenateBytes(buff, ByteHandler.stringToByteArray("***"));
        pw.write(buff);
        pw.flush();
    }
    
    private static void listGames (BufferedInputStream br, BufferedOutputStream pw, byte[] buff) throws IOException {
        sendFirstMessage(br, pw);
    }
    
    private static void exitNow (BufferedInputStream br, BufferedOutputStream pw) throws IOException {
        byte[] buff = ByteHandler.stringToByteArray("GOBYE***");
        pw.write(buff);
        pw.flush();
    }
    
    private static void unregGame (BufferedInputStream br, BufferedOutputStream pw) {
        try {
            int name_game = 40;
            String mess = "UNROK ";
            String mess2 = "***";
            byte[] buff = ByteHandler.stringToByteArray(mess);
            byte[] temp = new byte[1];
            temp[0] = ByteHandler.intToByte(name_game);
            buff = ByteHandler.concatenateBytes(buff, temp);
            buff = ByteHandler.concatenateBytes(buff, ByteHandler.stringToByteArray(mess2));
            pw.write(buff);
            pw.flush();
        }
        catch (Exception e) {
            System.err.println(e);
            e.printStackTrace();
        }
    }

    private static void playerMove (BufferedInputStream br, BufferedOutputStream pw, byte[] buff) {
        try {
            //[UPMOV d***]  [DOMOV d***]  [LEMOV d***]  [RIMOV d***]
            byte[] buff_temp = new byte[5];
            for (int i = 0; i < buff_temp.length; i++) {
                buff_temp[i] = buff[i];
            }
            String direzione = ByteHandler.stringFromByteArray(buff_temp);
            if (direzione.compareTo("UPMOV") == 0) { System.out.print ("movimento in alto"); }
            else if (direzione.compareTo("DOMOV") == 0) { System.out.print ("movimento in basso"); }
            else if (direzione.compareTo("LEMOV") == 0) { System.out.print ("movimento a sinistra"); }
            else if (direzione.compareTo("RIMOV") == 0) { System.out.print ("movimento a destra"); }

            buff_temp = new byte[3];
            buff_temp[0] = buff[6];
            buff_temp[1] = buff[7];
            buff_temp[2] = buff[8];
            String passi = ByteHandler.stringFromByteArray(buff_temp);

            System.out.println("  di " + passi + " passi");

            //Mex da inviare in risposta
            String mess = "MOVE! 009 004***";
            buff = ByteHandler.stringToByteArray(mess);
            pw.write(buff);
            pw.flush();
        }
        catch (Exception e) {
            System.err.println(e);
            e.printStackTrace();
        }
    }

    private static void listPlayers (BufferedInputStream br, BufferedOutputStream pw) throws IOException {
        int s = 5;
        //[GLIS! s***]
        String mess1 = "GLIS! ";
        String mess2 = "***";
        byte[] buff = ByteHandler.stringToByteArray(mess1);
        buff = ByteHandler.concatenateBytes(buff, ByteHandler.intToByteArray(s, 1));
        buff = ByteHandler.concatenateBytes(buff, ByteHandler.stringToByteArray(mess2));
        pw.write(buff);
        pw.flush();

        //[GPLYR id x y p***]
        for (int i = 0; i < s; i++) {
            String id = RandomString.getRandomString(8);
            String x = "004";
            String y = "009";
            String p = "0892";
            String mess = "GPLYR " + id + " " + x + " " + y + " " + p + "***";
            buff = ByteHandler.stringToByteArray(mess);
            pw.write(buff);
            pw.flush();
        }
    }

    private static void messageToAll (BufferedInputStream br, BufferedOutputStream pw, byte[] buff) throws IOException {
        //[MALL? Mess***]
        int length = 0;
        while (ByteHandler.charFromByte(buff[length+6]) != '*' && (length+6) < buff.length) {
            length++;
        } 
        byte[] buff_temp = new byte[length];
        for (int i = 0; i < length; i++) {
            buff_temp[i] = buff[i+6];
        }
        String mess = ByteHandler.stringFromByteArray(buff_temp);
        System.out.println("Messaggio spedito: " + mess);

        //[MALL!***]
        buff = ByteHandler.stringToByteArray("MALL!***");
        pw.write(buff);
        pw.flush();
    }

    private static void messageToSinglePlayer (BufferedInputStream br, BufferedOutputStream pw, byte[] buff) throws IOException {

        byte[] buff_temp = new byte[8];
        for (int i = 0; i < buff_temp.length; i++) {
            buff_temp[i] = buff[i+6];
        }
        String id = ByteHandler.stringFromByteArray(buff_temp);

        int length = 0;
        while ((length+15) < buff.length && ByteHandler.charFromByte(buff[length+15]) != '*') {
            length++;
        } 
        buff_temp = new byte[length];
        for (int i = 0; i < buff_temp.length; i++) {
            buff_temp[i] = buff[i+15];
        }
        String mess = ByteHandler.stringFromByteArray(buff_temp);

        System.out.println("send to " + id + " mess: " + mess);

        //[SEND!***]
        buff = ByteHandler.stringToByteArray("SEND!***");
        pw.write(buff);
        pw.flush();
    }

    public static void main (String args[]) throws IOException {
        ServerSocket server = new ServerSocket(4242);
        Socket socket = server.accept();
        BufferedInputStream br = new BufferedInputStream(socket.getInputStream());
        BufferedOutputStream pw = new BufferedOutputStream(socket.getOutputStream());
        sendFirstMessage(br, pw);
        boolean control = true;
        Prova chat_pubblica = new Prova();
        new Thread(chat_pubblica).start();

        while (control) {
            byte[] buff = new byte[50];
            buff = printReceivedMessage(br, pw, buff.length);
            byte[] buff_temp = new byte[5];
            for (int i = 0; i < buff_temp.length; i++) {
                buff_temp[i] = buff[i];
            }
            String mess = ByteHandler.stringFromByteArray(buff_temp);
            if (mess.compareTo("REGIS") == 0) { joinGame(br, pw, buff); }
            else if (mess.compareTo("SIZE?") == 0) { infoGame(br, pw, buff); }
            else if (mess.compareTo("LIST?") == 0) { listPlayersSpecificGame(br, pw, buff); }
            else if (mess.compareTo("IQUIT") == 0) { exitNow(br, pw); control = false; }
            else if (mess.compareTo("UNREG") == 0) { unregGame(br, pw); }
            else if (mess.compareTo("GAME?") == 0) { listGames(br, pw, buff); }
            else if (mess.compareTo("NEWPL") == 0) { joinNewGame(br, pw); }
            else if (mess.compareTo("START") == 0) { sendFirstGameMessage(br, pw); sendSecondGameMessage(br, pw); }
            else if (mess.compareTo("UPMOV")==0 || mess.compareTo("DOMOV")==0 || mess.compareTo("LEMOV") == 0 || mess.compareTo("RIMOV") == 0) {
                playerMove(br, pw, buff);
            }
            else if (mess.compareTo("GLIS?") == 0) { listPlayers(br, pw); }
            else if (mess.compareTo("MALL?") == 0) { messageToAll (br, pw, buff); }
            else if (mess.compareTo("SEND?") == 0) { messageToSinglePlayer (br, pw, buff); }
        }

        br.close();
        pw.close();
        socket.close();
        server.close();
    }
}
