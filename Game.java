import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;
import java.net.*;

public class Game {

    private static GameInfo receivedWelcomeMessages (BufferedInputStream br, BufferedOutputStream pw) {
        try {
            //Receive [WELCO m h w f ip port***]
            byte[] buff = new byte [39];
            if (br.read(buff) < 0) {
                System.out.println("ERROR 040!\nError in receiving the package!\n");
                return (null);
            }
            byte[] buff_temp = new byte [5];
            for (int i = 0; i < buff_temp.length; i++) {
                buff_temp[i] = buff[i];
            }
            String mess = ByteHandler.stringFromByteArray(buff_temp);
            if (mess.compareTo("WELCO") != 0) {
                System.out.println("ERROR 041!\nError in receiving the package!\n");
                return (null);
            }

            //m=buff[6] h=buff[8,9]  w=buff[11,12]  f=buff[14]  ip=buff[16-30]  port=buff[32-35]
            int name_game = ByteHandler.intFromByte(buff[6]);
            buff_temp = new byte [2];
            buff_temp[0] = buff[8];   buff_temp[1] = buff[9];
            int height_field = ByteHandler.intFromByteArray(buff_temp);
            buff_temp = new byte [2];
            buff_temp[0] = buff[11];   buff_temp[1] = buff[12];
            int width_field = ByteHandler.intFromByteArray(buff_temp);
            buff_temp = new byte [15];
            for (int i = 0; i < buff_temp.length; i++) {
                buff_temp[i] = buff[i+16];
            }
            String temp = ByteHandler.stringFromByteArray(buff_temp);
            String ip_multidiffusion = "";
            int cont = 0;
            while (temp.charAt(cont) != '#' && cont < temp.length()) {
                ip_multidiffusion = ip_multidiffusion + temp.charAt(cont);
                cont++;
            }
            buff_temp = new byte [4];
            for (int i = 0; i < buff_temp.length; i++) {
                buff_temp[i] = buff[i+32];
            }
            int port_multidiffusion = Integer.parseInt(ByteHandler.stringFromByteArray(buff_temp));

            //Receive [POSIT id x y***]
            buff = new byte [25];
            if (br.read(buff) < 0) {
                System.out.println("ERROR 042!\nError in receiving the package!\n");
                return (null);
            }
            buff_temp = new byte [5];
            for (int i = 0; i < buff_temp.length; i++) {
                buff_temp[i] = buff[i];
            }
            mess = ByteHandler.stringFromByteArray(buff_temp);
            if (mess.compareTo("POSIT") != 0) {
                System.out.println("ERROR 043!\nError in receiving the package!\n");
                return (null);
            }

            //id=buff[6-13]   x=buff[15-17]   y=buff[19-21]
            buff_temp = new byte [8];
            for (int i = 0; i < buff_temp.length; i++) {
                buff_temp[i] = buff[i+6];
            }
            String id = ByteHandler.stringFromByteArray(buff_temp);
            buff_temp = new byte [3];
            buff_temp[0] = buff[15];
            buff_temp[1] = buff[16];
            buff_temp[2] = buff[17];
            temp = ByteHandler.stringFromByteArray(buff_temp);
            int pos_x = Integer.parseInt(temp);
            buff_temp = new byte [3];
            buff_temp[0] = buff[19];
            buff_temp[1] = buff[20];
            buff_temp[2] = buff[21];
            temp = ByteHandler.stringFromByteArray(buff_temp);
            int pos_y = Integer.parseInt(temp);

            //Create a new object GameInfo
            GameInfo gameinfo = new GameInfo(name_game, height_field, width_field, id, pos_x, pos_y, 0, ip_multidiffusion, port_multidiffusion);

            return gameinfo;
        }
        catch (Exception e) {
            System.out.println("ERROR!\nProblem on receiving the beginner messages!");
            System.err.println(e);
            return (null);
        }
    }
    
    private static boolean receivedPrivateMessage (byte[] buff, GameInfo info) {
        //[MESSP␣id2␣mess+++]
        //id2=buff[6-13]   mess=buff[15-]
        try {
            byte[] buff_temp = new byte [5];
            for (int i = 0; i < buff_temp.length; i++) {
                buff_temp[i] = buff[i];
            }
            String control = ByteHandler.stringFromByteArray(buff_temp);
            if (control.compareTo("MESSP") != 0) { 
                System.out.println("ERROR!\nReceived unexpected message!\n");
                return false;
            }
            buff_temp = new byte [8];
            for (int i = 0; i < buff_temp.length; i++) {
                buff_temp[i] = buff[i+6];
            }
            String id = ByteHandler.stringFromByteArray(buff_temp);
            int length = 0;
            while ((length+15 < buff.length) && (ByteHandler.charFromByte(buff[length+15]) != '+')) {
                length++;
            }
            buff_temp = new byte[length];
            for (int i = 0; i < buff_temp.length; i++) {
                buff_temp[i] = buff[i+15];
            }
            String mess = ByteHandler.stringFromByteArray(buff_temp);

            if (info.controlGameFinished()) { return false; }
            System.out.println(FileHandler.change_page);
            System.out.println(" *** PRIVATE MESSAGE RECEIVED! ***");
            System.out.println(id + " has sent you: " + mess);
            System.out.println(FileHandler.change_page);
            System.out.print("> ");

            return (true);
        }
        catch (Exception e) {
            System.out.println("ERROR!\nThere was a problem while receiving private message!");
            System.err.println(e);
            return (false);
        }
    }

    private static boolean ghostMoved (byte[] buff, GameInfo info) {
        //[GHOST x y+++]
        //x=buff[6-8]   y=buff[10-12]
        byte[] buff_temp = new byte[3];
        buff_temp[0] = buff[6];
        buff_temp[1] = buff[7];
        buff_temp[2] = buff[8];
        String temp = ByteHandler.stringFromByteArray(buff_temp);
        int pos_x = Integer.parseInt(temp);
        buff_temp = new byte [3];
        buff_temp[0] = buff[10];
        buff_temp[1] = buff[11];
        buff_temp[2] = buff[12];
        temp = ByteHandler.stringFromByteArray(buff_temp);
        int pos_y = Integer.parseInt(temp);
        if (info.controlGameFinished()) { return false; }
        System.out.print("\n\n *** GHOST MOVED!   new position: (" + pos_x + "," + pos_y + ") ***\n\n> ");
        return true;
    }
    
    private static boolean someoneScored (byte[] buff, GameInfo info) {
        //[SCORE id p x y+++] 
        //id=buff[6-13]   p=buff[15-18]   x=buff[20-22]   y=[24-26]
        byte[] buff_temp = new byte[8];
        for (int i = 0; i < buff_temp.length; i++) {
            buff_temp[i] = buff[i+6];
        }
        String id = ByteHandler.stringFromByteArray(buff_temp);

        buff_temp = new byte[4];
        for (int i = 0; i < buff_temp.length; i++) {
            buff_temp[i] = buff[i+15];
        }
        int points = Integer.parseInt(ByteHandler.stringFromByteArray(buff_temp));

        buff_temp = new byte[3];
        buff_temp[0] = buff[20];
        buff_temp[1] = buff[21];
        buff_temp[2] = buff[22];
        int pos_x = Integer.parseInt(ByteHandler.stringFromByteArray(buff_temp));

        buff_temp = new byte[3];
        buff_temp[0] = buff[24];
        buff_temp[1] = buff[25];
        buff_temp[2] = buff[26];
        int pos_y = Integer.parseInt(ByteHandler.stringFromByteArray(buff_temp));

        if (info.controlGameFinished()) { return false;}

        if (id.compareTo(info.getMyId()) == 0) { return true; }

        System.out.println("\n\n *** POINT SCORED! " + id + " caught the ghost in (" + pos_x + "," + pos_y + ") ***");
        System.out.print("     " + id + " has now " + points + " points!\n\n> ");
        
        return true;
    }
    
    private static boolean receivedPublicMessage (byte[] buff, GameInfo info) {
        //[MESSA id mess+++]
        //id=buff[6-13]   mess[15-]
        byte[] buff_temp = new byte[8];
        for (int i = 0; i < buff_temp.length; i++) {
            buff_temp[i] = buff[i+6];
        }
        String id = ByteHandler.stringFromByteArray(buff_temp);
        int length_message = 0;
        while (ByteHandler.stringFromByteArray(buff).charAt(length_message+15) != '+' && length_message+15 < buff.length) {
            length_message++;
        }
        buff_temp = new byte [length_message];
        for (int i = 0; i < length_message; i++) {
            buff_temp[i] = buff[i+15];
        }
        String mess = ByteHandler.stringFromByteArray(buff_temp);

        String s = info.fixMessage(id, mess);
        info.addMessToChat(s);

        if (info.controlGameFinished()) { return false; }
        if (id.compareTo(info.getMyId()) == 0) { return true; }
        System.out.print("\n\n *** RECEIVED PUBLIC MESSAGE! ***\n\n> ");
        return true;
    }

    private static boolean printWinner (byte[] buff, GameInfo info) {
        //[ENDGA id p+++]
        //id=buff[6-13]   p=buff[15-18]
        byte[] buff_temp = new byte[8];
        for (int i = 0; i < buff_temp.length; i++) {
            buff_temp[i] = buff[i+6];
        }
        String id = ByteHandler.stringFromByteArray(buff_temp);
        
        buff_temp = new byte[4];
        for (int i = 0; i < buff_temp.length; i++) {
            buff_temp[i] = buff[i+15];
        }
        int points = Integer.parseInt(ByteHandler.stringFromByteArray(buff_temp));
        System.out.println("\n\n *** MATCH FINISHED! ***");
        if (id.compareTo(info.getMyId()) == 0) {
            System.out.println("     YOU" + " wins with " + points + " points\n\n");
        }
        else {
            System.out.println("     " + id + " wins with " + points + " points\n\n");
        }

        return true;
    }
    
    private static boolean receivedMulticastMessage (byte[] buff, GameInfo info) {
        byte[] buff_temp = new byte [5];
        for (int i = 0; i  < buff_temp.length; i++) {
            buff_temp[i] = buff[i];
        }
        String keyword = ByteHandler.stringFromByteArray(buff_temp);
        if (keyword.compareTo("GHOST") == 0) {
            return (ghostMoved(buff, info));
        }
        else if (keyword.compareTo("SCORE") == 0) {
            return (someoneScored(buff, info));
        }
        else if (keyword.compareTo("MESSA") == 0) {
            return (receivedPublicMessage (buff, info));
        }
        else if (keyword.compareTo("ENDGA") == 0) {
            info.setGameFinished();
            return (printWinner(buff, info));
        }
        return false;
    }
    
    public static int game (BufferedInputStream br, BufferedOutputStream pw) {
        try {
            System.out.println(FileHandler.change_page + "PLAYING GAME\n");
            FileHandler.printFile("GameRules.txt");
            System.out.println();

            GameInfo info = receivedWelcomeMessages(br, pw);

            Thread t = new Thread(new PlayingGame(br, pw, info));
            t.start();

            Selector sel = Selector.open();
            NetworkInterface ni = NetworkInterface.getNetworkInterfaces().nextElement();
            DatagramChannel multicast = DatagramChannel.open(StandardProtocolFamily.INET);
            multicast.setOption(StandardSocketOptions.SO_REUSEADDR, true);
            multicast.bind(new InetSocketAddress(info.getPortMultidiffusion()));
            multicast.setOption(StandardSocketOptions.IP_MULTICAST_IF, ni);
            multicast.join(InetAddress.getByName(info.getIpMultidiffusion()), ni);
            multicast.configureBlocking(false);
            multicast.register(sel, SelectionKey.OP_READ);
            DatagramChannel udp = DatagramChannel.open();
            udp.configureBlocking(false);
            udp.bind(new InetSocketAddress(5555));
            udp.register(sel, SelectionKey.OP_READ);

            while (info.controlGameFinished() == false) {
                sel.select();
                Iterator<SelectionKey> it = sel.selectedKeys().iterator();
                while (it.hasNext() && !info.controlGameFinished()) {
                    SelectionKey sk = it.next();
                    it.remove();
                    if (sk.isReadable() && sk.channel() == multicast && !info.controlGameFinished()) {
                        ByteBuffer temp = ByteBuffer.allocate(290);
                        multicast.receive(temp);
                        byte[] buff = temp.array();
                        if (receivedMulticastMessage (buff, info) == false) { return (0); }
                    }
                    else if (sk.isReadable() && sk.channel() == udp && !info.controlGameFinished()) {
                        ByteBuffer temp = ByteBuffer.allocate(290);
                        udp.receive(temp);
                        byte[] buff = temp.array();
                        if (receivedPrivateMessage (buff, info) == false) { return (0); }
                    }
                }
            }
            
            udp.disconnect();
            udp.close();
            multicast.disconnect();
            multicast.close();
            sel.close();

            return (0);
        }
        catch (Exception e) {
            System.out.println("ERROR!\nError in messages while playing the game!");
            System.err.println(e);
            return (-1);
        }
    }
}
