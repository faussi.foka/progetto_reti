import java.io.*;
/*
import java.util.*;
import java.net.*;
import java.util.Arrays;
import java.net.DatagramSocket;
import java.net.DatagramPacket;
*/


public class Lobby {

    private static boolean wantToExit (BufferedInputStream br, BufferedOutputStream pw) {
        try {
            byte[] buff = ByteHandler.stringToByteArray("IQUIT***");
            pw.write(buff);
            pw.flush();
            buff = new byte [8];
            br.read(buff);
            String control = ByteHandler.stringFromByteArray(buff);
            if (control.compareTo("GOBYE***") == 0) {
                System.out.println ("You're disconnecting from the server!\n");
                return (true);
            }
            else {
                return (false);
            }
        }
        catch (Exception e) {
            System.err.println(e);
            return (true);
        }
    }
    
    private static boolean isInArray (int value, int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (value == array[i]) { return true; }
        }
        return false;
    }

    private static boolean infoSpecificGame (BufferedInputStream br, BufferedOutputStream pw, int name_game) {
        try {
            //Ask the size
            byte[] buff = ByteHandler.stringToByteArray("SIZE? ");
            buff = ByteHandler.concatenateBytes(buff, ByteHandler.intToByteArray(name_game, 1));
            buff = ByteHandler.concatenateBytes(buff, ByteHandler.stringToByteArray("***"));
            pw.write(buff);
            pw.flush();
            
            //Read and print the size
            buff = new byte [16];
            br.read(buff);
            byte[] buff_temp = new byte [5];
            for (int i = 0; i < 5; i++) {
                buff_temp[i] = buff[i];
            }
            String temp = ByteHandler.stringFromByteArray(buff_temp);
            if (temp.compareTo("DUNNO") == 0) {
                System.out.println("\tGame " + name_game + " doesn't exist!\n\n");
                return true;
            }
            else if (temp.compareTo("SIZE!") != 0) {
                System.err.println("ERROR 020!\nPackage received isn't expected!");
                return true;
            }
            System.out.print("Game " + ByteHandler.intFromByte(buff[6]));
            buff_temp = new byte[2];
            buff_temp[0] = buff[8];
            buff_temp[1] = buff[9];
            System.out.print("     height: " + ByteHandler.intFromByteArray(buff_temp));
            buff_temp = new byte[2];
            buff_temp[0] = buff[11];
            buff_temp[1] = buff[12];
            System.out.println("     width: " + ByteHandler.intFromByteArray(buff_temp));
            System.out.println();

            //Ask the list [LIST? m***]
            buff = ByteHandler.concatenateBytes(ByteHandler.stringToByteArray("LIST? "), ByteHandler.intToByteArray(name_game, 1));
            buff = ByteHandler.concatenateBytes(buff, ByteHandler.stringToByteArray("***"));
            pw.write(buff);
            pw.flush();

            //Read [LIST! m s***]
            buff = new byte [12];
            br.read(buff);
            buff_temp = new byte [5];
            for (int i = 0; i < 5; i++) {
                buff_temp[i] = buff[i];
            }
            temp = ByteHandler.stringFromByteArray(buff_temp);
            if (temp.compareTo("DUNNO") == 0) {
                System.out.println("\n\tGame " + name_game + " doesn't exist!");
                return true;
            }
            else if (temp.compareTo("LIST!") != 0) {
                System.err.println("ERROR 014!\nPackage received isn't expected!");
                return true;
            }

            if (name_game != ByteHandler.intFromByte(buff[6])) {
                System.out.println("ERROR 015!\nGame received missmatch!");
                return true;
            }
            int number_players = ByteHandler.intFromByte(buff[8]);

            //Read and print the list of players [PLAYR id***]
            System.out.println("\t\tLIST OF PLAYERS:");
            for (int i = 0; i < number_players; i++) {
                buff = new byte [17];
                br.read(buff);
                buff_temp = new byte [5];
                for (int j = 0; j < 5; j++) {
                    buff_temp[j] = buff[j];
                }
                temp = ByteHandler.stringFromByteArray(buff_temp);
                if (temp.compareTo("PLAYR") != 0) {
                    System.err.println("ERROR 016!\nPackage received isn't expected!");
                }
                buff_temp = new byte[8];
                for (int j = 0; j < 8; j++) {
                    buff_temp[j] = buff[j+6];
                }
                String id_player = ByteHandler.stringFromByteArray(buff_temp);
                System.out.println("\t\t- " + id_player);
            }
            System.out.println("\n");
            return true;
        }
        catch (Exception e) {
            System.err.println(e);
            return true;
        }
    }
    
    private static int whichGame (BufferedInputStream br, BufferedOutputStream pw, int[] list_games) {
        try {
            //Which game?
            BufferedReader kb = new BufferedReader(new InputStreamReader(System.in));
            boolean correct_input = false;
            String temp;
            Integer name_game = null;
            while (correct_input == false || name_game == null) {
                correct_input = true;
                System.out.println("\n\tWrite the number of the game");
                System.out.print("> ");
                temp = kb.readLine();
                if (RandomString.isANumber(temp)) {
                    name_game = Integer.parseInt(temp);
                    if (!isInArray(name_game, list_games) || name_game < 0 || name_game > 255) {
                        correct_input = false;
                    }
                }
                else {
                    correct_input = false;
                }
            }
            return (name_game);
        }
        catch (Exception e) {
            e.printStackTrace();
            return (0);
        }
    }

    private static boolean sizeFieldSpecificGame (BufferedInputStream br, BufferedOutputStream pw, int name_game) {
        try {
            //Ask the size
            byte[] buff = ByteHandler.stringToByteArray("SIZE? ");
            buff = ByteHandler.concatenateBytes(buff, ByteHandler.intToByteArray(name_game, 1));
            buff = ByteHandler.concatenateBytes(buff, ByteHandler.stringToByteArray("***"));
            pw.write(buff);
            pw.flush();

            //Read and print the size
            buff = new byte [16];
            br.read(buff);
            byte[] buff_temp = new byte [5];
            for (int i = 0; i < 5; i++) {
                buff_temp[i] = buff[i];
            }
            String temp = ByteHandler.stringFromByteArray(buff_temp);
            if (temp.compareTo("DUNNO") == 0) {
                System.out.println("\tGame " + name_game + " doesn't exist!\n\n");
                return true;
            }
            else if (temp.compareTo("SIZE!") != 0) {
                System.err.println("ERROR 020!\nPackage received isn't expected!");
                return true;
            }
            System.out.print("\nGAME " + ByteHandler.intFromByte(buff[6]));
            buff_temp = new byte [2];
            buff_temp[0] = buff [8];
            buff_temp[1] = buff [9];
            int height = ByteHandler.intFromByteArray(buff_temp);
            System.out.print("     height: " + height);
            buff_temp = new byte [2];
            buff_temp[0] = buff [11];
            buff_temp[1] = buff [12];
            int width = ByteHandler.intFromByteArray(buff_temp);
            System.out.println("     width: " + width);
            System.out.println("\n");
            return true;
        }
        catch (Exception e) {
            System.err.println(e);
            return true;
        }
    }

    private static boolean sizeField (BufferedInputStream br, BufferedOutputStream pw, int[] list_games) {
        try {
            int name_game = whichGame(br, pw, list_games);
            if (name_game == 0) { 
                System.out.println("ERROR!\nThere was a problem while choosing the game!\n");
                return (true);
            }
            return sizeFieldSpecificGame(br, pw, name_game);
        }
        catch (Exception e) {
            System.err.println(e);
            return true;
        }
    }

    private static boolean listPlayersSpecificGame (BufferedInputStream br, BufferedOutputStream pw, int name_game) {
        try {
            //Ask the list [LIST? m***]
            byte[] buff = ByteHandler.concatenateBytes(ByteHandler.stringToByteArray("LIST? "), ByteHandler.intToByteArray(name_game, 1));
            buff = ByteHandler.concatenateBytes(buff, ByteHandler.stringToByteArray("***"));
            pw.write(buff);
            pw.flush();

            //Read [LIST! m s***]
            buff = new byte [12];
            br.read(buff);
            byte[] buff_temp = new byte [5];
            for (int i = 0; i < 5; i++) {
                buff_temp[i] = buff[i];
            }
            String temp = ByteHandler.stringFromByteArray(buff_temp);
            if (temp.compareTo("DUNNO") == 0) {
                System.out.println("\n\tGame " + name_game + " doesn't exist!");
                return true;
            }
            else if (temp.compareTo("LIST!") != 0) {
                System.err.println("ERROR 014!\nPackage received isn't expected!");
                return true;
            }

            if (name_game != ByteHandler.intFromByte(buff[6])) {
                System.out.println("ERROR 015!\nGame received missmatch!");
                return true;
            }
            int number_players = ByteHandler.intFromByte(buff[8]);

            //Read and print the list of players [PLAYR id***]
            System.out.println("\t\tLIST OF PLAYERS:");
            for (int i = 0; i < number_players; i++) {
                buff = new byte [17];
                br.read(buff);
                buff_temp = new byte [5];
                for (int j = 0; j < 5; j++) {
                    buff_temp[j] = buff[j];
                }
                temp = ByteHandler.stringFromByteArray(buff_temp);
                if (temp.compareTo("PLAYR") != 0) {
                    System.err.println("ERROR 016!\nPackage received isn't expected!");
                }
                buff_temp = new byte[8];
                for (int j = 0; j < 8; j++) {
                    buff_temp[j] = buff[j+6];
                }
                String id_player = ByteHandler.stringFromByteArray(buff_temp);
                System.out.println("\t\t- " + id_player);
            }
            System.out.println("\n");
            return true;
        }
        catch (Exception e) {
            System.err.println(e);
            return true;
        }
    }

    private static boolean listPlayers (BufferedInputStream br, BufferedOutputStream pw, int[] list_games) {
        try {
            int name_game = whichGame(br, pw, list_games);
            if (name_game == 0) { 
                System.out.println("ERROR!\nThere was a problem while choosing the game!\n");
                return (true);
            }

            return listPlayersSpecificGame (br, pw, name_game);
        }
        catch (Exception e) {
            System.err.println(e);
            return true;
        }
    }
    
    private static int joinNewGame (BufferedInputStream br, BufferedOutputStream pw, int[] list_games) {
        try {
            String id = RandomString.getRandomString(8);
            id = id + " ";
            String port = "5555";
            String end_message = "***";
            String mess = "NEWPL " + id + port + end_message;
            byte[] buff = ByteHandler.stringToByteArray(mess);
            pw.write(buff);
            pw.flush();

            buff = new byte [10];
            if (br.read(buff) < 0) {
                System.out.println ("ERROR 010!\nError in receiving the package!\n");
                return (-1);
            }
            byte[] buff_temp = new byte [5];
            for (int i = 0; i < buff_temp.length; i++) {
                buff_temp[i] = buff[i];
            }
            String control = ByteHandler.stringFromByteArray(buff_temp);
            if (control.compareTo("REGNO") == 0) {
                return (-1);
            }
            else if (control.compareTo("REGOK") == 0) {
                System.out.println("You have created a new game!");
                int name_game = ByteHandler.intFromByte(buff[6]);
                return (name_game);
            }
            else {
                System.out.println ("ERROR 010!\nError in receiving the package!\n");
                return (-1);
            }
        }
        catch (Exception e) {
            System.err.println(e);
            return (-1);
        }
    }
    
    private static int joinGame (BufferedInputStream br, BufferedOutputStream pw, int[] list_games) {
        try {
            BufferedReader kb = new BufferedReader(new InputStreamReader(System.in));
            String id = RandomString.getRandomString(8) + " ";
            String port = "5555 ";
            String mess = "REGIS " + id + port;
            byte[] buff = ByteHandler.stringToByteArray(mess);
            boolean correct_input = false;
            String temp;
            Integer name_game = null;
            while (correct_input == false || name_game == null) {
                correct_input = true;
                System.out.println("\n\tWrite the number of the game");
                System.out.print("> ");
                temp = kb.readLine();
                if (RandomString.isANumber(temp)) {
                    name_game = Integer.parseInt(temp);
                    if (!isInArray(name_game, list_games) || name_game < 0 || name_game > 255) {
                        correct_input = false;
                    }
                }
                else {
                    correct_input = false;
                }
            }
            buff = ByteHandler.concatenateBytes(buff, ByteHandler.intToByteArray(name_game, 1));
            buff = ByteHandler.concatenateBytes(buff, ByteHandler.stringToByteArray("***"));
            pw.write(buff);
            pw.flush();
            
            buff = new byte [10];
            if (br.read(buff) < 0) {
                System.out.println ("ERROR 010!\nError in receiving the package!\n");
                return (-1);
            }
            byte[] buff_temp = new byte [5];
            for (int i = 0; i < buff_temp.length; i++) {
                buff_temp[i] = buff[i];
            }
            temp = ByteHandler.stringFromByteArray(buff_temp);
            if (temp.compareTo("REGNO") == 0) {
                return (-1);
            }
            else if (temp.compareTo("REGOK") == 0) {
                if (name_game != ByteHandler.intFromByte(buff[6])) {
                    System.err.println("ERROR 013!\nUnexpected error with the server.\n\n");
                    wantToExit(br, pw);
                    return (-1);
                }
                System.out.println("\nYou are in the game " + name_game);
                return (name_game);
            }
            else {
                System.out.println ("ERROR 010!\nError in receiving the package!\n");
                return (-1);
            }
        }
        catch (Exception e) {
            System.err.println(e);
            return (-1);
        }
    }

    //Return the name of the game
    private static int recvOGAME (BufferedInputStream br, boolean print_list_games) {
        try {
            byte[] buff = new byte[12];
            if (br.read(buff) < 0) {
                System.out.println ("ERROR 002!\nError in receiving the package!\n");
                return (-1);
            }

            //Control if I've received the correct message
            byte[] temp = new byte[5];
            for (int i = 0; i < temp.length; i++) {
                temp[i] = buff[i];
            }
            String keyword = ByteHandler.stringFromByteArray(temp);
            temp = null;
            if (keyword.compareTo("OGAME") != 0) {
                System.out.println("ERROR 001!\nMessage received unexpected!\n");
                return(-1);
            }

            //m = buff[6]   s = buff[8]
            int game_name = ByteHandler.intFromByte(buff[6]);
            if (print_list_games) {
                int number_players = ByteHandler.intFromByte(buff[8]);
                System.out.println("\t\t- game " + game_name + ": " + number_players + " players");
            }

            return (game_name);
        }

        catch (Exception e) {
            System.err.println(e + "\n");
            return (-1);
        }
    }

    private static int[] listGames (BufferedInputStream br, BufferedOutputStream pw, boolean print_list_games) {
        try {
            String mess = "GAME?***";
            byte[] buff = ByteHandler.stringToByteArray(mess);
            pw.write(buff);
            pw.flush();
            return (recvListGames(br, print_list_games));
        }
        catch (Exception e) {
            System.err.println(e);
            return null;
        }
    }
    
    private static int[] recvListGames (BufferedInputStream br, boolean print_list_games) {
        try {
            byte[] buff = new byte[10];
            if (br.read(buff) < 0) {
                System.out.println ("ERROR 003!\nError in receiving the package!\n");
                return (null);
            }
        
            byte[] temp = new byte[5];
            for (int i = 0; i < temp.length; i++) {
                temp[i] = buff[i];
            }
            String keyword = ByteHandler.stringFromByteArray(temp);
            temp = null;
            if (keyword.compareTo("GAMES") != 0) {
                System.out.println("ERROR 004!\nMessage received unexpected!\n");
                return(null);
            }
            temp = new byte[3];
            temp[0] = buff[7];
            temp[1] = buff[8];
            temp[2] = buff[9];
            keyword = ByteHandler.stringFromByteArray(temp);
            if (!ByteHandler.messEndCorrectly(temp)) {
                System.out.println("ERROR 005!\nMessage received unexpected!\n");
                return(null);
            }
            temp = null;

            int number_games = ByteHandler.intFromByte(buff[6]);
            int[] list_games = new int [number_games];
            if (print_list_games) {
                System.out.println("\t\tHERE BELOW THE LIST OF PENDING GAMES:");
            }
            for (int i = 0; i < number_games; i++) {
                list_games[i] = recvOGAME(br, print_list_games);
            }
        
            System.out.println("\n");
            return (list_games);
        }

        catch (Exception e) {
            System.err.println(e + "\n");
            return (null);
        }
    }
    
    private static boolean unregGame (BufferedInputStream br, BufferedOutputStream pw, int name_game) {
        try {
            String mess = "UNREG***";
            byte[] buff = ByteHandler.stringToByteArray(mess);
            pw.write(buff);
            pw.flush();
            buff = new byte[10];
            br.read(buff);
            byte[] buff_temp = new byte[5];
            for (int i = 0; i < buff_temp.length; i++) {
                buff_temp[i] = buff[i];
            }
            String received_mess = ByteHandler.stringFromByteArray(buff_temp);
            if (received_mess.compareTo("UNROK") == 0 && name_game == ByteHandler.intFromByte(buff[6])) {
                return true;
            }
            return false;
        }
        catch (Exception e) {
            System.err.println(e);
            return false;
        }
    }

    
    public static int game (BufferedInputStream br, BufferedOutputStream pw) {
        try {
            System.out.println(FileHandler.change_page + "LOBBY\n");
            int[] list_games = recvListGames(br, true);

            while (true) {
                System.out.println("\t type 'j' to join a game.");
                System.out.println("\t type 'n' to create a new game.");
                System.out.println("\t type 'h' if you need an help.");
                System.out.print("\n> ");
                BufferedReader kb = new BufferedReader(new InputStreamReader(System.in));
                String temp = kb.readLine();
                char key_pressed;
                if (temp.length() > 0) {
                    key_pressed = temp.charAt(0);
                }
                else {
                    key_pressed = ' ';
                }
                temp = null;
                key_pressed = Character.toLowerCase(key_pressed);
                int name_game;
                switch (key_pressed) {
                    case ('j'):
                        name_game = joinGame(br, pw, list_games);
                        if (name_game >= 0) {
                            int control = waitingGame (br, pw, name_game);
                            if (control < 0) {
                                System.err.println("ERROR 011!\nC'e stato un problema durante la partita");
                            }
                            return (0);
                        }
                        else {
                            System.out.println("Non sei stato iscritto alla partita.\n");
                        }
                        System.out.println(FileHandler.change_page + "LOBBY\n");
                        list_games = listGames(br, pw, true);
                        break;
                    
                    case ('n'):
                        name_game = joinNewGame(br, pw, list_games);
                        if (name_game >= 0) {
                            int control = waitingGame (br, pw, name_game);
                            if (control < 0) {
                                System.err.println("ERROR 011!\nC'e stato un problema durante la partita");
                            }
                        }
                        else {
                            System.out.println("Non è stata creata la nuova partita.\n");
                        }
                        return (0);
                        
                    case ('p'):
                        listPlayers(br, pw, list_games);
                        break;

                    case ('f'):
                        sizeField (br, pw, list_games);
                        break;

                    case ('l'):
                        list_games = listGames(br, pw, true);
                        temp = null;
                        break;

                    case ('h'):
                        System.out.println();
                        FileHandler.printFile("Lobby_guide.txt");
                        break;

                    case ('e'):
                        if (wantToExit (br, pw)) {
                            return (0);
                        }
                        break;

                    default:
                        System.out.println("This command doesn't exist\nPlease click 'h' to learn how to play.\n");
                        break;
                }
            }
        }
        
        catch (Exception e) {
            System.err.println(e + "\n");
            return (-1);
        }
    }

    private static int waitingGame (BufferedInputStream br, BufferedOutputStream pw, int name_game) {
        try {
            System.out.println(FileHandler.change_page + "WAITING GAME\n");
            infoSpecificGame(br, pw, name_game);
            while (true) {
                System.out.println();
                System.out.println("\t type 's' when you're ready.");
                System.out.println("\t type 'h' if you need an help.");
                System.out.print("\n> ");
                BufferedReader kb = new BufferedReader(new InputStreamReader(System.in));
                String temp = kb.readLine();
                char key_pressed;
                if (temp.length() > 0) {
                    key_pressed = temp.charAt(0);
                }
                else {
                    key_pressed = ' ';
                }
                temp = null;
                key_pressed = Character.toLowerCase(key_pressed);
                switch (key_pressed) {
                    case ('e'):
                        boolean control = unregGame(br, pw, name_game);
                        if (control == false) {
                            System.err.println("\nERROR 020!\nYou've got a problem with the server.");
                            return (-1);
                        }
                        return (0);

                    case ('p'):
                        listPlayersSpecificGame(br, pw, name_game);
                        break;
                    
                    case ('f'):
                        sizeFieldSpecificGame(br, pw, name_game);
                        break;
            
                    case ('s'):
                        String mess = "START***";
                        byte[] buff = ByteHandler.stringToByteArray(mess);
                        pw.write(buff);
                        pw.flush();
                        int check = Game.game(br, pw);
                        if (check < 0) {
                            System.err.println("There was an error during the game!\n");
                        }
                        return (0);
                    case ('h'):
                        FileHandler.printFile("WaitingGame_guide.txt");
                        break;
                    default:
                        System.out.println("This command doesn't exist\nPlease click 'h' to learn how to play.\n");
                        break;
                }
            }
        }
        catch (Exception e) {
            System.err.println(e);
            return (-1);
        }
    }


}
