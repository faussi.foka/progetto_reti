import java.io.*;

public class FileHandler {

    //  LIST OF FILES:
    // "Welcome.txt"
    // "Home_guide.txt"
    // "Lobby_guide.txt"
    // "WaitingGame_guide.txt"

    public static String change_page = "\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n\t\t\t";

    public static void printFile (String filename) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            String line = br.readLine();
            while (line != null) {
                System.out.println(line);
                line = br.readLine();
            }
            br.close();
            System.out.println();
        }
        catch (Exception e) {
            System.err.println(e);
            System.out.println("\n");
        }
    }

}
