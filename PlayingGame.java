
import java.io.*;

public class PlayingGame implements Runnable {

    private static BufferedInputStream br;
    private static BufferedOutputStream pw;
    private static GameInfo info;

    public PlayingGame (BufferedInputStream i, BufferedOutputStream o, GameInfo g) {
        br = i;
        pw = o;
        info = g;
    }

    private static boolean receiveNewPosition () {
        try {
            //Receive [MOVE! x y***] or [MOVEF x y p***]
            //x=buff[6-8]   y=buff[10-12]   p=buff[14-17]
            byte[] buff = new byte [21];
            br.read(buff);
            byte[] buff_temp = new byte [5];
            for (int i = 0; i < buff_temp.length; i++) {
                buff_temp[i] = buff[i];
            }
            String temp = ByteHandler.stringFromByteArray(buff_temp);
            if (temp.compareTo("MOVE!") == 0) {
                int pos_x, pos_y;

                buff_temp = new byte [3];
                buff_temp[0] = buff[6];
                buff_temp[1] = buff[7];
                buff_temp[2] = buff[8];
                pos_x = Integer.parseInt(ByteHandler.stringFromByteArray(buff_temp));

                buff_temp = new byte [3];
                buff_temp[0] = buff[10];
                buff_temp[1] = buff[11];
                buff_temp[2] = buff[12];
                pos_y = Integer.parseInt(ByteHandler.stringFromByteArray(buff_temp));

                info.setPosition(pos_x, pos_y);

                return (true);
            }
            else if (temp.compareTo("MOVEF") == 0) {
                int pos_x, pos_y;

                buff_temp = new byte [3];
                buff_temp[0] = buff[6];
                buff_temp[1] = buff[7];
                buff_temp[2] = buff[8];
                pos_x = Integer.parseInt(ByteHandler.stringFromByteArray(buff_temp));

                buff_temp = new byte [3];
                buff_temp[0] = buff[10];
                buff_temp[1] = buff[11];
                buff_temp[2] = buff[12];
                pos_y = Integer.parseInt(ByteHandler.stringFromByteArray(buff_temp));

                info.setPosition(pos_x, pos_y);

                buff_temp = new byte [4];
                for (int i = 0; i < buff_temp.length; i++) {
                    buff_temp[i] = buff[i+14];
                } 
                info.setPoints(Integer.parseInt(ByteHandler.stringFromByteArray(buff_temp)));

                System.out.println("\n   YOU'VE CAUGHT A GHOST!\n");
                
                return (true);
            }
            else if (temp.compareTo("GOBYE") == 0) {
                System.out.println("\n\tGAME FINISHED!\n");
                return (false);
            }
            else {
                System.out.println("Error!\nError during a movement\n\n");
                return (false);
            }
        }
        catch (Exception e) {
            System.out.println("Error!\nProblem on receiving the new position!");
            System.out.println(e + "\n");
            return (false);
        }
    }

    private static boolean move (char direction, String steps) {
        try {
            //Send [UPMOV d***]
            String mess;
            switch (direction) {
                case ('u'):
                    mess = "UPMOV ";
                    break;
                case ('d'):
                    mess = "DOMOV ";
                    break;
                case ('l'):
                    mess = "LEMOV ";
                    break;
                case ('r'):
                    mess = "RIMOV ";
                    break;
                default:
                    System.out.println("ERROR!\nLogic problem on sending the move\n");
                    return (false);
            }
            String zeros;
            if (steps.length() == 0) { zeros = "000"; }
            else if (steps.length() == 1) { zeros = "00"; }
            else if (steps.length() == 2) { zeros = "0"; }
            else if (steps.length() == 3) { zeros = ""; }
            else {
                System.out.println("\nThere is a logic error in 'move' function!\n\n");
                return (false);
            }
            mess = mess + zeros + steps + "***";
            byte[] buff = ByteHandler.stringToByteArray(mess);
            pw.write(buff);
            pw.flush();
            return (receiveNewPosition());
        }
        catch (Exception e) {
            System.out.println("\nError!\nError on 'move' function!\n\n");
            return (false);
        }
    }
    
    private static void wantToExit () {
        try {
            byte[] buff = ByteHandler.stringToByteArray("IQUIT***");
            pw.write(buff);
            pw.flush();
            buff = new byte[8];
            br.read(buff);
            String mess = ByteHandler.stringFromByteArray(buff);
            if (mess.compareTo("GOBYE***") == 0) {
                System.out.println("\nQuitting the game...\n");
            }
            else {
                System.out.println("\nThere is a problem while quitting the game\n");
            }
        }
        catch (Exception e) {
            System.out.println("ERROR!\nLogic error while quitting");
        }
    }

    private static boolean infoPlayers (int number_players) {
        try {
            System.out.println(FileHandler.change_page + "LIST OF PLAYERS\n");

            //Receive 'number_players' times [GPLYR id x y p***]
            //id=buff[6-13]   x=buff[15-17]   y=buff[19-21]   p=buff[23-26]
            for (int i = 0; i < number_players; i++) {
                byte[] buff = new byte[30];
                br.read(buff);

                byte[] buff_temp = new byte[5];
                for (int j = 0; j < buff_temp.length; j++) {
                    buff_temp[j] = buff[j];
                }
                String temp = ByteHandler.stringFromByteArray(buff_temp);
                if (temp.compareTo("GOBYE") == 0) {
                    System.out.println("\n\tGAME FINISHED!\n");
                    return (false);
                }
                if (temp.compareTo("GPLYR") != 0) {
                    System.out.println("\nERROR!\nMessage received unexpected!\n\n");
                    return (false);
                }

                buff_temp = new byte[8];
                for (int j = 0; j < buff_temp.length; j++) {
                    buff_temp[j] = buff[j+6];
                }
                String player_id = ByteHandler.stringFromByteArray(buff_temp);
                if (player_id.compareTo(info.getMyId()) == 0) { player_id = "YOU"; }

                buff_temp = new byte[3];
                buff_temp[0] = buff[15];
                buff_temp[1] = buff[16];
                buff_temp[2] = buff[17];
                temp = ByteHandler.stringFromByteArray(buff_temp);
                int x = Integer.parseInt(temp);

                buff_temp = new byte[3];
                buff_temp[0] = buff[19];
                buff_temp[1] = buff[20];
                buff_temp[2] = buff[21];
                temp = ByteHandler.stringFromByteArray(buff_temp);
                int y = Integer.parseInt(temp);

                buff_temp = new byte[4];
                buff_temp[0] = buff[23];
                buff_temp[1] = buff[24];
                buff_temp[2] = buff[25];
                buff_temp[3] = buff[26];
                temp = ByteHandler.stringFromByteArray(buff_temp);
                int p = Integer.parseInt(temp);

                System.out.println("\t- " + player_id + "   " + p + " points   (" + x + "," + y + ")");
            }
            System.out.println(FileHandler.change_page);
            return (true);
        }
        catch (Exception e) {
            System.out.println("ERROR!\nError while receiving the ranking!");
            return (false);
        }
    }

    private static boolean listPlayers () {
        try {
            byte[] buff = ByteHandler.stringToByteArray("GLIS?***");
            pw.write(buff);
            pw.flush();

            buff = new byte[10];
            br.read(buff);
            byte[] buff_temp = new byte [5];
            for (int i = 0; i < buff_temp.length; i++) {
                buff_temp[i] = buff[i];
            }
            String temp = ByteHandler.stringFromByteArray(buff_temp);
            if (temp.compareTo("GOBYE") == 0) {
                System.out.println("\n\tGAME FINISHED!\n");
                return (false);
            }
            else if (temp.compareTo("GLIS!") == 0) {
                int number_players = ByteHandler.intFromByte(buff[6]);
                return (infoPlayers(number_players));
            }
            return (false);
        }
        catch (Exception e) {
            System.out.println("ERROR!\nError while asking the list of players!\n");
            return (false);
        }
    }

    private static String[] getIdPlayers () {
        try {
            byte[] buff = ByteHandler.stringToByteArray("GLIS?***");
            pw.write(buff);
            pw.flush();

            buff = new byte [10];
            br.read(buff);
            byte[] buff_temp = new byte [5];
            for (int i = 0; i < buff_temp.length; i++) {
                buff_temp[i] = buff[i];
            }
            String temp = ByteHandler.stringFromByteArray(buff_temp);
            if (temp.compareTo("GLIS!") != 0) {
                System.out.println("Error receiving the list of players!\n\n");
                return (null);
            }
            int number_players = ByteHandler.intFromByte(buff[6]);

            String[] id_players = new String [number_players];
            for (int i = 0; i < number_players; i++) {
                buff = new byte[30];
                br.read(buff);

                buff_temp = new byte[5];
                for (int j = 0; j < buff_temp.length; j++) {
                    buff_temp[j] = buff[j];
                }
                temp = ByteHandler.stringFromByteArray(buff_temp);
                if (temp.compareTo("GPLYR") != 0) {
                    System.out.println("\nERROR!\nMessage received unexpected!\n\n");
                    return (null);
                }
                buff_temp = new byte[8];
                for (int j = 0; j < buff_temp.length; j++) {
                    buff_temp[j] = buff[j+6];
                }
                id_players[i] = ByteHandler.stringFromByteArray(buff_temp);
            }

            return (id_players);
        }
        catch (Exception e) {
            System.out.println("ERROR!\nProblem while receiving the list of players!\n");
            return (null);
        }
    }

    private static boolean sendPrivateMessage (String receiver, BufferedReader kb) {
        try {
            //Send [SEND? id mess***]
            System.out.print("Write message: ");
            String text = kb.readLine();
            if (text.length() > 200) {
                text = text.substring(0, 200);
            }
            String mess = "SEND? " + receiver + " " + text + "***";
            byte[] buff = ByteHandler.stringToByteArray(mess);
            pw.write(buff);
            pw.flush();

            //Receive [SEND!***] or [NSEND***]
            buff = new byte [8];
            br.read(buff);
            mess = ByteHandler.stringFromByteArray(buff);
            if (mess.compareTo("SEND!***") == 0) {
                System.out.println("\nYour message has been sent!");
                System.out.println(FileHandler.change_page);
                return (true);
            }
            else if (mess.compareTo("NSEND***") == 0) {
                System.out.println("\nYour sending message failed!");
                System.out.println(FileHandler.change_page);
                return (true);
            }
            else if (mess.compareTo("GOBYE") == 0) {
                System.out.println("\n\tGAME FINISHED!\n");
                return false;
            }
            else {
                System.out.println("\nERROR!\nMessage received unexpected!\n\n");
                return false;
            }
        }
        catch (Exception e) {
            System.err.println("\nERROR!\nError sending a private message!\n");
            return (false);
        }
    }

    private static boolean sendPublicMessage (BufferedReader kb) {
        try {
            //Send [MALL? Mess***]
            System.out.print("Write message: ");
            String text = kb.readLine();
            if (text.length() > 200) {
                text = text.substring(0, 200);
            }
            String mess = "MALL? " + text + "***";
            byte[] buff = ByteHandler.stringToByteArray(mess);
            pw.write(buff);
            pw.flush();

            //Receive [MALL!***] 
            buff = new byte[8];
            br.read(buff);
            mess = ByteHandler.stringFromByteArray(buff);
            if (mess.compareTo("GOBYE***") == 0) {
                System.out.println("\n\tGAME FINISHED!\n");
                return (false);
            }
            if (mess.compareTo("MALL!***") != 0) {
                return (false);
            }
            System.out.println("Your message has been sent!");
            System.out.println(FileHandler.change_page);

            return (true);
        }
        catch (Exception e) {
            System.out.println("\nERROR!\nError sending a public message!\n");
            return (false);
        }
    }

    private static boolean sendMessage (BufferedReader kb) {
        try {
            System.out.println("\n'p' to send a private message OR 'a' to send a message to all other players");
            System.out.print("> ");
            String temp = kb.readLine();
            temp = temp.toLowerCase();
            while (temp.length() < 1 || (temp.charAt(0)!='p' && temp.charAt(0)!='a')) {
                System.out.println("Incorrect input.");
                System.out.println("\n'p' to send a private message OR 'a' to send a message to all other players");
                System.out.println("> ");
                temp = kb.readLine();
            }
            char key_pressed = temp.charAt(0);

            if (key_pressed == 'p') {
                String[] id_players = getIdPlayers();
                System.out.println("\n  Who do you want to text?");
                for (int i = 0; i < id_players.length; i++) {
                    System.out.println((i+1) + ") " + id_players[i]);
                }
                System.out.print("> ");
                temp = kb.readLine();
                while (RandomString.isANumber(temp) == false || Integer.parseInt(temp) < 1 || Integer.parseInt(temp) > id_players.length) {
                    System.out.print("> ");
                    temp = kb.readLine();
                }

                String id_receiver = id_players[Integer.parseInt(temp) - 1];
                return (sendPrivateMessage (id_receiver, kb));
            }
            else if (key_pressed == 'a') {
                return (sendPublicMessage (kb));
            }
            else {
                System.out.println("\nError unexpected while sending your message!\n\n");
                return false;
            }
        }
        catch (Exception e) {
            System.out.println("\nError while sending your message!\n");
            System.err.println(e + "\n");
            return false;
        }
    }

    private static void printPublicChat () {
        String[] chat = info.getPublicChat();
        System.out.println(FileHandler.change_page + "PUBLIC CHAT\n");
        if (chat.length == 0) {
            System.out.println (" empty chat\n" + FileHandler.change_page); 
        }
        else {
            for (int i = 0; i < chat.length; i++) {
                System.out.println(chat[i]);
            }
            System.out.println(FileHandler.change_page);
        }
    }

    public void run() {
        try {
            while (info.controlGameFinished() == false) {
                System.out.println("   GAME " + info.getNameGame());
                int[] field = info.getDimensionField();
                System.out.println("   height: " + field[0] + "    width: " + field[1] + "\n");
                System.out.println("   YOU: " + info.getMyId());
                int[] position = info.getPosition();
                System.out.println("   (" + position[0] + "," + position[1] + ")" + "   " + info.getPoints() + " points\n");
                System.out.print("> ");
                BufferedReader kb = new BufferedReader(new InputStreamReader(System.in));
                String temp = kb.readLine();
                char key_pressed;
                if (temp.length() > 0) {
                    key_pressed = temp.charAt(0);
                }
                else {
                    key_pressed = ' ';
                }
                key_pressed = Character.toLowerCase(key_pressed);

                if (key_pressed == 'u' || key_pressed == 'd' || key_pressed == 'l' || key_pressed == 'r') {
                    int check = -1;
                    String steps = "";
                    while (check < 0 || check > 999) {
                        System.out.print("number steps: ");
                        steps = kb.readLine();
                        if (steps.length() > 0 && RandomString.isANumber(steps)) {
                            check = Integer.parseInt(steps);
                        }
                    }
                    if (move(key_pressed, steps) == false) {
                        info.setGameFinished();
                    }
                    System.out.println();
                }
                else if (key_pressed == 'e') {
                    info.setGameFinished();
                    wantToExit();
                }
                else if (key_pressed == 'p') {
                    if (listPlayers() == false) {
                        info.setGameFinished();
                        System.out.println();
                    }
                }
                else if (key_pressed == 'm') {
                    if (sendMessage (kb) == false) {
                        info.setGameFinished();
                        System.out.println();
                    }
                }
                else if (key_pressed == 'c') {
                    printPublicChat ();
                }
                else if (key_pressed == 'h') {
                    System.out.println();
                    FileHandler.printFile("GameRules.txt");
                }
                else {System.out.println("\nThis command doesn't exist\nPlease click 'h' to learn how to play.\n"); }
            }
        }
        catch (Exception e) {
            System.err.println(e);
        }
    }
}
