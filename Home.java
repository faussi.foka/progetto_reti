import java.io.*;
//import java.util.*;
import java.net.*;

public class Home {

    private static void wantToPlay () {
        try {
            Socket socket = new Socket("localhost", 4242);
            BufferedInputStream in_stream = new BufferedInputStream(socket.getInputStream());
            BufferedOutputStream out_stream = new BufferedOutputStream(socket.getOutputStream());
            int control = Lobby.game(in_stream, out_stream); //Lobby class take control
            in_stream.close();
            out_stream.close();
            socket.close();
            if (control >= 0) {
                System.out.println("\nConnection with the server closed properly!\n\n");
            }
            else {
                System.out.println("\nThere was a problem on the connection with the server!\n\n");
            }
        }
        catch (Exception e) {
            System.err.println(e + "\n");
        }
    }

    private static boolean wantToExit (BufferedReader kb) {
        try {
            System.out.println("\n\tAre you sure you want to exit the game?");
            System.out.println("\tWrite 'yes' to close.");
            System.out.println("\tOtherwise type anything else to continue.");
            System.out.print("\n> ");
            String text_pressed = kb.readLine();
            text_pressed = text_pressed.toLowerCase();
            if (text_pressed.compareTo("yes") == 0) {
                System.out.println("\n\tSee you soon!\n");
                return (true);
            }
            else {
                System.out.println("\n\tYou've made the correct decision!\n");
                return (false);
            }
        }

        catch (Exception e) {
            System.err.println(e + "\n");
            return (true);
        }
    }

    public static int game () {
        try {
            while (true) {
                System.out.println(FileHandler.change_page + "HOME\n");
                System.out.println("\t type '1' to start.");
                System.out.println("\t type 'h' if you need an help.");
                System.out.print("\n> ");
                BufferedReader kb = new BufferedReader(new InputStreamReader(System.in));
                String temp = kb.readLine();
                char key_pressed;
                if (temp.length() > 0) {
                    key_pressed = temp.charAt(0);
                }
                else {
                    key_pressed = ' ';
                }
                temp = null;
                key_pressed = Character.toLowerCase(key_pressed);
                switch (key_pressed) {
                    case ('1'):
                        wantToPlay();
                        break;
                
                    case ('e'):
                        boolean b = wantToExit(kb);
                        if (b) {
                            return (0);
                        }
                        break;
                
                    case ('h'):
                        System.out.println();
                        FileHandler.printFile("Home_guide.txt");
                        break;
                
                    default:
                        System.out.println("This command doesn't exist\nPlease click 'h' to learn how to play.\n");
                }
            }
        }

        catch (Exception e) {
            System.err.println(e + "\n");
            return (-1);
        }
    }
}
