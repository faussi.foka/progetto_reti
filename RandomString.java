import java.util.Random;


public class RandomString {
    
    private static String upper_alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static String lower_alphabet = upper_alphabet.toLowerCase();
    private static String number = "0123456789";
    private static String all_characters = upper_alphabet + lower_alphabet + number;

    public static char getRandomCharacter () {
        Random rand = new Random();
        int max = all_characters.length();
        int random_number = rand.nextInt(max);

        char c = all_characters.charAt(random_number);
        return c;
    }

    public static String getRandomString (int length) {
        String string = "";

        for (int i=0; i<length; i++) {
            char c = getRandomCharacter();
            string = string + c;
        }

        return string;
    }

    public static boolean isANumber (String s) {
        int length = s.length();
        for (int i = 0; i < length; i++) {
            String temp = s.substring(i, i+1);
            if (!number.contains(temp)) { return (false); }
        }
        return true;
    }
}