public class GameInfo {
    private int name_game;
    private int height_field;
    private int width_field;
    private String id;
    private int pos_x;
    private int pos_y;
    private int points;
    private String[] public_chat;
    private boolean is_finished;
    private String ip_multidiffusion;
    private int port_multidiffusion;

    private static final int capacity_chat = 20;

    public GameInfo (int _name, int _height, int _width, String _id, int _x, int _y, int _points, String _ip_multidiffusion, int _port_multidiffusion) {
        name_game = _name;
        height_field = _height;
        width_field = _width;
        id = _id;
        pos_x = _x;
        pos_y = _y;
        points = _points;
        public_chat = new String [0];
        is_finished = false;
        ip_multidiffusion = _ip_multidiffusion;
        port_multidiffusion = _port_multidiffusion;
    }

    //SETTER
    public synchronized void setPosition (int x, int y) {
        pos_x = x;
        pos_y = y;
    }
    public synchronized void setPoints (int p) { points = p; }
    public synchronized void addMessToChat (String mess) {
        int length = public_chat.length;
        if (length < capacity_chat) {
            length++;
            String[] new_public_chat = new String [length];
            for (int i = 0; i < public_chat.length; i++) {
                new_public_chat[i] = public_chat[i];
            }
            new_public_chat[length-1] = mess;
            public_chat = new_public_chat;
        }
        else {
            for (int i = 0; i < public_chat.length - 1; i++) {
                public_chat[i] = public_chat[i+1];
            }
            public_chat[public_chat.length - 1] = mess;
        }
    }
    public synchronized void setGameFinished () { is_finished = true; }

    //GETTER
    public int getNameGame () { return name_game; }
    public int[] getDimensionField () {
        int[] dimension = new int [2];
        dimension[0] = height_field;
        dimension[1] = width_field;
        return dimension;
    }
    public String getMyId () { return id; }
    public synchronized int[] getPosition () {
        int[] position = new int [2];
        position[0] = pos_x;
        position[1] = pos_y;
        return position;
    }
    public synchronized int getPoints () { return points; }
    public synchronized String[] getPublicChat () { return public_chat; }
    public synchronized boolean controlGameFinished () { return is_finished; }
    public String getIpMultidiffusion () { return ip_multidiffusion; }
    public int getPortMultidiffusion () { return port_multidiffusion; }

    public String fixMessage (String _id, String mess) {
        String final_message = new String();
        String begin = new String();
        if (id.compareTo(_id) == 0) { begin = "YOU: "; }
        else { begin = _id + ": "; }
        final_message = begin + mess;
        return final_message;
    }
}
