# Progetto_Reti

Progetto del corso di Programmation Reseaux (programmazione di reti) svolto e consegnato durante il periodo di attivtà Erasmus nell'università di Paris-Diderot.
Il lavoro è stato svolto in un gruppo di 3 persone: i due colleghi si sono concentrati sul lato Server del gioco, mentre io sul lato Client.
In questa repo è presente solo il lato Client. 

## Caccia ai fantasmi

Un gioco multiplayer online dove diversi giocatori iscritti ad una partita devono muoversi "alla cieca" alla ricerca dei fantasmi sparsi per la mappa. Anche la mappa, come i fantasmi, non è visibile all'utente sin da subito, ma la scopre muovendosi e scontrandosi contro i muri.
Catturare un fantasma fa guadagnare punti
La partita finisce quando tutti i fantasmi presenti nella stanza vengono catturati.
Vince il giocatore che possiede più punti al termine della partita.
