import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.io.*;
import java.util.*;

/*
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
*/

public class Prova implements Runnable {

    private void sendMessageToAll (DatagramSocket dso, BufferedReader kb) {
        try {
            System.out.print("Write message: ");
            String keyboard = kb.readLine();
            //MESSA id mess+++
            String id = RandomString.getRandomString(8);
            String mess = "MESSA " + id + " " + keyboard + "+++";
            byte[] buff = ByteHandler.stringToByteArray(mess);
            InetSocketAddress ia = new InetSocketAddress("225.1.0.99", 9999);
            DatagramPacket packet = new DatagramPacket(buff, buff.length, ia);
            dso.send(packet);
        }
        catch (Exception e) {
            System.err.println(e);
            e.printStackTrace();
        }
    }

    private void ghostMoved (DatagramSocket dso) {
        try {
            //[GHOST x y+++]
            int max = 9;
            Random r = new Random();
            int temp = r.nextInt(max + 1);
            String x = "00" + Integer.toString(temp);
            max = 8;
            r = new Random();
            temp = r.nextInt(max + 1);
            String y = "00" + Integer.toString(temp);

            String mess = "GHOST " + x + " " + y + "+++";
            byte[] buff = ByteHandler.stringToByteArray(mess);
            InetSocketAddress ia = new InetSocketAddress("225.1.0.99", 9999);
            DatagramPacket packet = new DatagramPacket(buff, buff.length, ia);
            dso.send(packet);
        }
        catch (Exception e) {
            System.err.println(e);
            e.printStackTrace();
        }
    }

    private void playerScored (DatagramSocket dso) {
        try {
            //[SCORE id p x y+++]
            String id = RandomString.getRandomString(8);
            Random r = new Random();
            int max = 999;
            int min = 100;
            int temp = r.nextInt(max-min) + min;
            String p = "0" + Integer.toString(temp);
            max = 10;
            temp = r.nextInt(max);
            String x = "00" + Integer.toString(temp);
            max = 8;
            temp = r.nextInt(max);
            String y = "00" + Integer.toString(temp);

            String mess = "SCORE " + id + " " + p + " " + x + " " + y + "+++";
            byte[] buff = ByteHandler.stringToByteArray(mess);
            InetSocketAddress ia = new InetSocketAddress("225.1.0.99", 9999);
            DatagramPacket packet = new DatagramPacket(buff, buff.length, ia);

            //System.out.println("Sto mandando: " + ByteHandler.stringFromByteArray(buff));

            dso.send(packet);
        }
        catch (Exception e) {
            System.err.println(e);
            e.printStackTrace();
        }
    }

    private void endGame (DatagramSocket dso) {
        //[ENDGA id p+++]
        try {
            String id = RandomString.getRandomString(8);
            Random r = new Random();
            int max = 999;
            int min = 100;
            int temp = r.nextInt(max-min) + max;
            String p = "0" + Integer.toString(temp);

            String mess = "ENDGA " + id + " " + p + "+++";
            byte[] buff = ByteHandler.stringToByteArray(mess);
            InetSocketAddress ia = new InetSocketAddress("225.1.0.99", 9999);
            DatagramPacket packet = new DatagramPacket(buff, buff.length, ia);
            dso.send(packet);
        }
        catch (Exception e) {
            System.err.println(e);
            e.printStackTrace();
        }
    }

    public void run() {
        try {
            Thread.sleep(500);
            DatagramSocket dso = new DatagramSocket();
            boolean control = true;
            while (control) {
                System.out.println("M) Message   G) Ghost   S) Score   E) Endgame");
                System.out.print("Which type of message? ");
                BufferedReader kb = new BufferedReader(new InputStreamReader(System.in));
                String keyword = kb.readLine();
                char tasto;
                if (keyword.length() == 0) {
                    tasto = ' ';
                }
                else {
                    keyword = keyword.toLowerCase();
                    tasto = keyword.charAt(0);
                }
                switch (tasto) {
                    case ('m'):
                        sendMessageToAll(dso, kb);
                        break;
                    case ('g'):
                        ghostMoved(dso);
                        break;
                    case ('s'):
                        playerScored(dso);
                        break;
                    case ('e'):
                        control = false;
                        endGame(dso);
                        dso.close();
                        break;
                }
            }
        }
        catch (Exception e) {
            System.err.println(e);
        }
    }
}
